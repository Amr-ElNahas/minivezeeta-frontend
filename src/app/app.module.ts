import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { DropdownDirective } from './shared/dropdown.directive';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserProfileMenuComponent } from './user-profile/user-profile-menu/user-profile-menu.component';
import { UserProfileManageComponent } from './user-profile/user-profile-manage/user-profile-manage.component';
import { UserProfileChangePasswordComponent } from './user-profile/user-profile-change-password/user-profile-change-password.component';
import { SearchHeaderComponent } from './search-header/search-header.component';
import { SearchHeaderBarComponent } from './search-header/search-header-bar/search-header-bar.component';
import { AlertComponent } from './shared/alert/alert.component';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';
import { ErrorComponent } from './error/error.component';
import { SearchResultContainerComponent } from './search-header/search-result-container/search-result-container.component';
import { SearchResultListComponent } from './search-header/search-result-list/search-result-list.component';
import { SearchResultItemComponent } from './search-header/search-result-list/search-result-item/search-result-item.component';
import { BookingContainerComponent } from './search-header/search-result-list/search-result-item/booking-container/booking-container.component';
import { BookingItemComponent } from './search-header/search-result-list/search-result-item/booking-container/booking-item/booking-item.component';
import { AreaFilterPipePipe } from './shared/area-filter-pipe.pipe';
import { SpecialtiesFilterPipePipe } from './shared/specialties-filter-pipe.pipe';
import { MyAppointmentContainerComponent } from './my-appointment-container/my-appointment-container.component';
import { MyAppointmentListComponent } from './my-appointment-container/my-appointment-list/my-appointment-list.component';
import { MyAppointmentItemComponent } from './my-appointment-container/my-appointment-list/my-appointment-item/my-appointment-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DropdownDirective,
    UserProfileComponent,
    UserProfileMenuComponent,
    UserProfileManageComponent,
    UserProfileChangePasswordComponent,
    SearchHeaderComponent,
    SearchHeaderBarComponent,
    AlertComponent,
    LoadingSpinnerComponent,
    ErrorComponent,
    SearchResultContainerComponent,
    SearchResultListComponent,
    SearchResultItemComponent,
    BookingContainerComponent,
    BookingItemComponent,
    AreaFilterPipePipe,
    SpecialtiesFilterPipePipe,
    MyAppointmentContainerComponent,
    MyAppointmentListComponent,
    MyAppointmentItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
