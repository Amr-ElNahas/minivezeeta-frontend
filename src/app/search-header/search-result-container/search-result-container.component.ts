import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-result-container',
  templateUrl: './search-result-container.component.html',
  styleUrls: ['./search-result-container.component.css']
})
export class SearchResultContainerComponent implements OnInit {

  @Input() specialtyFilter:string=''
  @Input() areaFilter:string=''
  @Input() nameFilter:string=''

  constructor() { }

  ngOnInit(): void {
  }

}
