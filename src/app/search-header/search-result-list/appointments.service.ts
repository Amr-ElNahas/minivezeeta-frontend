import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Appointment } from "src/app/shared/appointment.model";

@Injectable({
    providedIn: 'root'
})
export class AppointmentsService{
    appointments:Appointment[]=[]
    appointmentsChanged=new Subject<Appointment[]>()

    getAppointment(id:number){
        return this.appointments[id]
    }

    getAppointments(){
        return this.appointments.slice()
    }

    setAppointments(appointments:Appointment[]){
        this.appointments=appointments
        this.appointmentsChanged.next(appointments)
    }
}