import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Doctor } from 'src/app/shared/doctor.model';
import { DoctorsService } from './doctorsService.service';

@Component({
  selector: 'app-search-result-list',
  templateUrl: './search-result-list.component.html',
  styleUrls: ['./search-result-list.component.css']
})
export class SearchResultListComponent implements OnInit, OnDestroy {

  doctors:Doctor[]=[]
  subscription!: Subscription
  @Input() specialtyFilter:string=''
  @Input() areaFilter:string=''
  @Input() nameFilter:string=''

  constructor(private doctorsService:DoctorsService) { }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.doctors=this.doctorsService.getDoctors()
    this.subscription=this.doctorsService.doctorsChanged.subscribe((doctors)=>{
      this.doctors=doctors;
    })
  }

}
