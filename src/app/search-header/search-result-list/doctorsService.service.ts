import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Doctor } from "src/app/shared/doctor.model";

@Injectable({
    providedIn: 'root'
})
export class DoctorsService{
    doctors:Doctor[]=[]
    doctorsChanged=new Subject<Doctor[]>()

    getDoctor(id:number){
        return this.doctors[id]
    }

    getDoctors(){
        return this.doctors.slice()
    }

    setDoctors(doctors:Doctor[]){
        this.doctors=doctors
        this.doctorsChanged.next(doctors)
    }
}