import { Component, Injectable, Input, OnInit } from '@angular/core';
import { Doctor } from 'src/app/shared/doctor.model';

@Component({
  selector: 'app-search-result-item',
  templateUrl: './search-result-item.component.html',
  styleUrls: ['./search-result-item.component.css']
})
export class SearchResultItemComponent implements OnInit {

  @Input()
  doctor!: Doctor;

  @Input()
  index!: number;

  constructor() { }

  ngOnInit(): void {
  }

}
