import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Appointment } from 'src/app/shared/appointment.model';
import { DataStorageService } from 'src/app/shared/data-storage.service';
import { AppointmentsService } from '../../../appointments.service';

@Component({
  selector: 'app-booking-item',
  templateUrl: './booking-item.component.html',
  styleUrls: ['./booking-item.component.css']
})
export class BookingItemComponent implements OnInit, OnDestroy {
  
  @Input() appointment!: Appointment;
  patientId:number
  role:string
  comment:any
  // subscription!: Subscription
  constructor(private authService:AuthService,private dataStorageService:DataStorageService,private appointmentsService:AppointmentsService) { 
    this.patientId=authService.uid
    this.role=authService.role
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    
  }
  book(){
    this.appointment.isBooked=true
    this.appointment.comments=this.comment
    this.appointment.patientId=this.patientId
    this.dataStorageService.bookAppointment(this.appointment,this.patientId)
  }
  unbook(){
    
    this.appointment.isBooked=false
    this.appointment.comments=null
    this.dataStorageService.unbookAppointment(this.appointment,this.patientId)
  }

}
