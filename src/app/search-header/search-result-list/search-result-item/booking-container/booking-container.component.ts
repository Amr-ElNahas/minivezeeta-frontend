import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Appointment } from 'src/app/shared/appointment.model';
import { AppointmentsService } from '../../appointments.service';
import { DoctorsService } from '../../doctorsService.service';

@Component({
  selector: 'app-booking-container',
  templateUrl: './booking-container.component.html',
  styleUrls: ['./booking-container.component.css']
})
export class BookingContainerComponent implements OnInit, OnDestroy {

  @Input()
  appointments!: Appointment[];
  // appointments:Appointment[]=[]
  // subscription!: Subscription
  constructor(private appointmentsService:AppointmentsService) { }

  ngOnDestroy(): void {
    
  }

  ngOnInit(): void {
    // this.appointments=this.appointmentsService.getDoctorsAppointments()
    // this.subscription=this.appointmentsService.appointmentsChanged.subscribe((appointments)=>{
    //   this.appointments=appointments;
    // })
  }

}
