import { Component, OnInit } from '@angular/core';
import { DataStorageService } from 'src/app/shared/data-storage.service';

@Component({
  selector: 'app-search-header-bar',
  templateUrl: './search-header-bar.component.html',
  styleUrls: ['./search-header-bar.component.css']
})
export class SearchHeaderBarComponent implements OnInit {

  specialtyFilter:string=''
  areaFilter:string=''
  nameFilter:string=''

  constructor(private dataStorageService:DataStorageService) { }
  


  ngOnInit(): void {
    
  }
  onSubmit(){
    this.dataStorageService.fetchDoctors()
    
  }
}
