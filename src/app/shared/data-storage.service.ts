import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { DoctorsService } from "../search-header/search-result-list/doctorsService.service";
import { Doctor } from "./doctor.model";
import {tap} from 'rxjs/operators';
import { AppointmentsService } from "../search-header/search-result-list/appointments.service";
import { Appointment } from "./appointment.model";

@Injectable({
    providedIn:'root'
})
export class DataStorageService{
    constructor(private http:HttpClient, private doctorsService:DoctorsService, private appointmentsService:AppointmentsService){

    }

    fetchDoctors(){
        console.log('fetching')
        this.http.get<Doctor[]>('http://localhost:8082/api/doctors')
        //.pipe(tap(doctors=>this.doctorsService.setDoctors(doctors)))
        .subscribe((doctors)=>{
            this.doctorsService.setDoctors(doctors)
            console.log(doctors)
        })
    }

    bookAppointment(appointment:Appointment,pid:number){
        const tempApp:Appointment=new Appointment(appointment.id,undefined,appointment.dateTime,appointment.comments,appointment.isBooked)
        this.http.put('http://localhost:8082/api/appointments/patients/'+pid+'/book',tempApp).subscribe();
    }
    unbookAppointment(appointment:Appointment,pid:number){
        if(appointment.patientId!==pid){
            return
        }
        const tempApp:Appointment=new Appointment(appointment.id,undefined,appointment.dateTime,appointment.comments,appointment.isBooked)
        this.http.put('http://localhost:8082/api/appointments/patients/'+pid+'/unbook',tempApp).subscribe();
    }
}