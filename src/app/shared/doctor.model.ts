import { Appointment } from "./appointment.model";
import { Specialty } from "./specialty.model";

export class Doctor {
    constructor(public id: number,public specialties:Specialty[], public appointmentsSet:Appointment[], public appointmentsRecieved:number[], public username:string,public dateOdBirth:Date,public gender:string,public firstName:string,public lastName:string, public phoneNumber:string,public fees:number,public waitingTime:Date,public area:string,public entity:string,public entityName:string) {
    }
  }
  
  /*
this.active = active;
		this.password = password;
		this.roles = roles;
		this.username = username;
		this.area = area;
		this.dateOfBirth = dateOfBirth;
		this.entity = entity;
		this.entityName = entityName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fees = fees;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		this.waitingTime = waitingTime;
  */