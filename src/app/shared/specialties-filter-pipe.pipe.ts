import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'specialtiesFilterPipe'
})
export class SpecialtiesFilterPipePipe implements PipeTransform {

  transform(value: any, filterString:string, propName:string) {
    if(value.length===0){
      return value
    }
    const resultArray=[]
    for(const item of value){
   OUT:   for(const item2 of item[propName]){
        if(item2.title.toLowerCase().includes(filterString.toLowerCase())){
          resultArray.push(item)
          break OUT
        }
      }
    }
    return resultArray
  }

}
