import { Directive, ElementRef, HostBinding, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  @HostBinding("class.open") isOpen = false
  constructor(private elRef: ElementRef, private rend: Renderer2) { }

  @HostListener('click') mouseClick() {
    //this.rend.addClass(this.elRef.nativeElement, "open")
    this.isOpen = !this.isOpen
  }
}
