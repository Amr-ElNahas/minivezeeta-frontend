import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'areaFilterPipe'
})
export class AreaFilterPipePipe implements PipeTransform {

  transform(value: any, filterString:string, propName:string) {
    if(value.length===0){
      return value
    }
    const resultArray=[]
    for(const item of value){
      
      if(item[propName].toLowerCase().includes(filterString.toLowerCase())){
        resultArray.push(item)
      }
    }
    return resultArray
  }

}
