export class Appointment {
    constructor(public id: number,public patientId:number|undefined,public dateTime:Date, public comments:string|null,public isBooked:boolean) {
    }
}
  