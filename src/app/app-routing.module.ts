import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorComponent } from './error/error.component';
import { SearchHeaderBarComponent } from './search-header/search-header-bar/search-header-bar.component';
import { SearchHeaderComponent } from './search-header/search-header.component';
import { UserProfileChangePasswordComponent } from './user-profile/user-profile-change-password/user-profile-change-password.component';
import { UserProfileManageComponent } from './user-profile/user-profile-manage/user-profile-manage.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

const routes: Routes = [
  {path:'myprofile', component: UserProfileComponent , children:[
    {path:'manageprofile',component: UserProfileManageComponent},
    {path:'changepassword',component: UserProfileChangePasswordComponent}
  ]
},
{path:'search', component:SearchHeaderComponent},
{path:'', redirectTo:'/search',pathMatch: 'full'},
{path:'**', component:ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
